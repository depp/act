Translations
============

Translations will be placed in this folder when running::

    python manage.py makemessages --ignore venv --ignore act/templates/account --ignore act/users
