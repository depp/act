import inspect
import random
import sys

from django.core import management
from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from factory import Faker

from act import meetings
from act.meetings.models import (
    Issue,
    Lobbying,
    Meeting,
    Organization,
    Person,
    PostType,
    Representative,
)
from act.meetings.tests.factories import (
    IssueFactory,
    MeetingFactory,
    OrganizationFactory,
    PersonFactory,
    PostTypeFactory,
    RepresentativeFactory,
)


class Command(BaseCommand):
    """
    Fake data are generated for development purposes.
    Use --db-reset explicitely, in order to generate new data.

    """

    help = "Generate a batch of fake data to test views and templates (development phase, only)"

    def add_arguments(self, parser):
        parser.add_argument(
            "--fixtures-file",
            "-f",
            dest="fixtures_file",
            help="Path to the fixtures file. If not set, fixtures are not generated.",
        )
        parser.add_argument(
            "--db-reset",
            dest="db_reset",
            action="store_true",
            help="Reset data in the DB, in order to generate new data",
        )
        parser.add_argument(
            "--size",
            dest="size",
            type=tuple,
            default=(200, 40, 50),
            help="Number of meetings, representatives, lobbyists",
        )

    def handle(self, *args, **options):

        fixtures_file = options["fixtures_file"]
        db_reset = options["db_reset"]

        n_meetings, n_representatives, n_lobbyists = options["size"]

        if db_reset:

            message = (
                "WARNING: This task will remove all data from the database.\n\n"
                "Type 'yes' to continue, or 'no' to cancel: "
            )
            if input(message) != "yes":
                raise CommandError("Fake data generation canceled.")

            cursor = connection.cursor()
            for name, obj in inspect.getmembers(meetings.models):
                if inspect.isclass(obj) and hasattr(obj, "_meta"):
                    cursor.execute(
                        f"TRUNCATE {obj._meta.db_table} RESTART IDENTITY CASCADE;"
                    )
                    sys.stdout.write(
                        f"removed {obj.objects.count()} {obj._meta.verbose_name_plural}\n"
                    )

        for i in range(0, n_lobbyists):
            PersonFactory.create(gender=random.choice(["m", "f", "nb"]))
        sys.stdout.write(
            f"generated {n_lobbyists} {Person._meta.verbose_name_plural}\n"
        )

        n_organizations = int(n_lobbyists / random.randint(2, 5))
        for i in range(0, n_organizations):
            OrganizationFactory.create(org_type=random.choice(["PRI", "PUB", "LOB"]))
        sys.stdout.write(
            f"generated {n_organizations} {Organization._meta.verbose_name_plural}\n"
        )

        for k, v in [
            ("Sindaco", 10),
            ("Vicesindaco", 9),
            ("Assessore", 5),
            ("Pesidente del Consiglio", 2),
            ("Consigliere", 1),
        ]:
            PostTypeFactory.create(descr=k, priority=v)
        sys.stdout.write(f"generated 5 {PostType._meta.verbose_name_plural}\n")

        for name in [
            "Trasporti",
            "Salute",
            "Educazione",
            "Ambiente",
            "Cultura",
            "Turismo",
            "Tasse",
        ]:
            IssueFactory.create(name=name)
        sys.stdout.write(f"generated 7 {Issue._meta.verbose_name_plural}\n")

        for i in range(0, n_representatives):
            random_index = random.randint(0, PostType.objects.count() - 1)
            RepresentativeFactory.create()
        sys.stdout.write(
            f"generated {n_representatives} {Representative._meta.verbose_name_plural}\n"
        )

        for j in range(0, n_meetings):
            lobbyists = []
            for k in range(0, random.randint(1, 3)):
                p_random_index = random.randint(0, Person.objects.count() - 1)
                p = Person.objects.all()[p_random_index]

                o_random_index = random.randint(0, Organization.objects.count() - 1)
                if random.choice([0, 1]):
                    o = Organization.objects.all()[o_random_index]
                else:
                    o = None

                lobbying, created = Lobbying.objects.get_or_create(
                    person=p, organization=o
                )
                lobbyists.append(lobbying)

            representatives = set()
            for k in range(0, random.randint(1, 4)):
                random_index = random.randint(0, Representative.objects.count() - 1)
                r = Representative.objects.all()[random_index]
                representatives.add(r)
            representatives = list(representatives)

            issues = set()
            for k in range(0, random.randint(0, 3)):
                random_index = random.randint(0, Issue.objects.count() - 1)
                i = Issue.objects.all()[random_index]
                issues.add(i)
            issues = list(issues)

            if random.random() > 0.75:
                meeting_date = Faker("future_date", end_date="+60d")
            else:
                meeting_date = Faker("date_between", start_date="-300d")
            MeetingFactory.create(
                lobbyists=lobbyists,
                representatives=representatives,
                issues=issues,
                date=meeting_date,
            )

        sys.stdout.write(
            f"generated {n_meetings} {Meeting._meta.verbose_name_plural}\n"
        )

        if fixtures_file:
            management.call_command(
                "dumpdata",
                "meetings",
                format="json",
                indent=4,
                stdout=self.stdout,
                output=fixtures_file,
            )

            sys.stdout.write(f"generated data exported into {fixtures_file}\n")

        sys.stdout.write("Finishing")
