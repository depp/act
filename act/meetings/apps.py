from django.apps import AppConfig


class MeetingsConfig(AppConfig):
    name = "act.meetings"
