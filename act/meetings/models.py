from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from act.meetings.mixins import MeetingsDetailsMixin
from act.utils.validators import (
    partial_date_validator,
    validate_income,
    validate_partial_date,
    validate_percentage,
)


class Person(models.Model):
    """A person doing lobbying activity.

    A person is only identified through name and gender,
    she may be lobbying for one or more organizations, or on her own.
    """

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        unique_together = ("family_name", "given_name")

    url_name = "person_detail"

    given_name = models.CharField(
        verbose_name=_("given name"),
        max_length=128,
        blank=True,
        null=True,
        help_text=_("The primary given name"),
    )

    family_name = models.CharField(
        verbose_name=_("family name"),
        max_length=128,
        blank=True,
        null=True,
        help_text=_("The family name"),
    )

    gender = models.CharField(
        verbose_name=_("gender"),
        max_length=32,
        blank=True,
        null=True,
        db_index=True,
        help_text=_("A gender"),
    )

    @property
    def name(self):
        return f"{self.given_name} {self.family_name}"

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f"{self.given_name} {self.family_name} (PK:{self.pk})"


class Organization(MeetingsDetailsMixin, models.Model):
    """An organization involved in lobbying activity.

    Organizations always lobby through persons.
    """

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")

    url_name = "organization_detail"

    name = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        help_text=_("The organization name"),
        unique=True,
    )

    class OrgTypes(models.TextChoices):
        ORG_PRI = (
            "PRI",
            _("Private company"),
        )
        ORG_PUB = (
            "PUB",
            _("Public sector company"),
        )
        ORG_LOB = (
            "LOB",
            _("Lobbying company"),
        )

    org_type = models.CharField(
        _("organization type"),
        max_length=3,
        choices=OrgTypes.choices,
        help_text=_("The organization type"),
    )

    persons_lobbying = models.ManyToManyField(
        to="Person",
        through="Lobbying",
        through_fields=("organization", "person"),
        related_name="lobbying_for_organizations",
    )

    @property
    def meetings(self):
        return Meeting.objects.filter(lobbyists__organization=self)

    @property
    def representatives_met(self):
        return Representative.objects.filter(
            meetings__date__lt=now(), meetings__lobbyists__organization=self
        ).distinct()

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f"{self.name} ({self.get_org_type_display()}) (PK:{self.pk})"


class Lobbying(models.Model):
    """A lobbying activity is done by a person on her own, or while representing an organization.

    The ``organization`` field may be null, indicating that the lobbying activity is performed
    by the person on her own.
    """

    class Meta:
        verbose_name = _("Lobbying activity")
        verbose_name_plural = _("Lobbying activities")

    url_name = "lobbying_detail"

    person = models.ForeignKey(
        to="Person",
        blank=False,
        null=False,
        related_name="lobbyings",
        verbose_name=_("Person"),
        help_text=_("The person who is doing the lobbying activity"),
        on_delete=models.CASCADE,
    )

    organization = models.ForeignKey(
        to="Organization",
        blank=True,
        null=True,
        related_name="lobbyings",
        verbose_name=_("Organization"),
        help_text=_(
            "The organization for which the person is doing the lobbying activity"
        ),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        if self.organization:
            return f"{self.person} for {self.organization} (PK: {self.pk})"
        else:
            return f"{self.person} (PK: {self.pk})"


class PostType(models.Model):
    """A post type"""

    class Meta:
        verbose_name = _("Post type")
        verbose_name_plural = _("Post types")

    descr = models.CharField(
        verbose_name=_("name"),
        max_length=128,
        help_text=_("The post type"),
        unique=True,
    )

    priority = models.PositiveIntegerField(
        verbose_name=_("Priority"),
        blank=True,
        null=True,
        help_text=_("The priority of this post"),
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return self.descr


class Representative(MeetingsDetailsMixin, models.Model):
    """A politician is an elected representative, with one post
    in some legislative or government city institutions, worth lobbying for.

    When more than one posts are available, only the main one is reported.

    A politician has some detailed info, on declared income, company ownerships and posts.
    """

    class Meta:
        verbose_name = _("Representative")
        verbose_name_plural = _("Representatives")
        unique_together = ("family_name", "given_name")

    url_name = "representative_detail"

    given_name = models.CharField(
        verbose_name=_("given name"),
        max_length=128,
        blank=True,
        null=True,
        help_text=_("The primary given name"),
    )

    family_name = models.CharField(
        verbose_name=_("family name"),
        max_length=128,
        blank=True,
        null=True,
        help_text=_("The family name"),
    )

    gender = models.CharField(
        verbose_name=_("gender"),
        max_length=32,
        blank=True,
        null=True,
        db_index=True,
        help_text=_("A gender"),
    )

    class AgeClasses(models.TextChoices):
        AGE_18_35 = (
            "18_35",
            "18 - 35",
        )
        AGE_36_45 = (
            "36_45",
            "36 - 45",
        )
        AGE_46_55 = (
            "46_55",
            "46 - 55",
        )
        AGE_56_65 = (
            "56_65",
            "56 - 65",
        )
        AGE_OVR65 = (
            "OVR65",
            _("Over 65"),
        )

    age_class = models.CharField(
        _("age class"),
        max_length=5,
        choices=AgeClasses.choices,
        help_text=_("The age class"),
    )

    party = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        help_text=_(
            "The political party this representative is affiliated to, if any."
        ),
    )

    post_type = models.ForeignKey(
        to="PostType",
        null=True,
        related_name="representatives",
        verbose_name=_("Post"),
        help_text=_("The representative's post"),
        on_delete=models.CASCADE,
    )

    post_start_date = models.CharField(
        _("start date"),
        max_length=10,
        blank=True,
        null=True,
        validators=[partial_date_validator, validate_partial_date],
        help_text=_("The date when the post has started"),
    )

    post_end_date = models.CharField(
        _("end date"),
        max_length=10,
        blank=True,
        null=True,
        validators=[partial_date_validator, validate_partial_date],
        help_text=_("The date when the post has ended"),
    )

    picture = models.URLField(
        verbose_name=_("picture"),
        blank=True,
        null=True,
        help_text=_("A URL of a head shot"),
    )

    last_income = models.FloatField(
        _("Last declared income"),
        blank=True,
        null=True,
        validators=[validate_income],
    )

    @property
    def name(self):
        return f"{self.given_name} {self.family_name}"

    @property
    def is_current(self):
        return self.post_end_date is None

    @property
    def n_interests(self):
        return len(
            set(self.shares.values_list("company_name", flat=True))
            | set(self.posts.values_list("company_name", flat=True))
        )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f"Representative {self.given_name} {self.family_name} (PK:{self.pk})"


class CompanyShare(models.Model):
    """A share in a private company."""

    class Meta:
        verbose_name = _("Company Share")
        verbose_name_plural = _("Company Shares")

    url_name = "company_share_detail"

    representative = models.ForeignKey(
        to="Representative",
        related_name="shares",
        verbose_name=_("Representative"),
        help_text=_("The owner of the share"),
        on_delete=models.CASCADE,
    )

    company_name = models.CharField(
        _("company name"),
        max_length=256,
        help_text=_("The company name"),
    )

    share_percentage = models.FloatField(
        validators=[validate_percentage],
        help_text=_(
            "The *required* percentage of shares owned, "
            "expressed as a floating number, from 0 to 100."
        ),
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return (
            f"{self.representative} owns {round(self.share_percentage, 2)}% "
            f"of {self.company_name} (PK: {self.pk})"
        )


class CompanyPost(models.Model):
    """A post in a private company"""

    class Meta:
        verbose_name = _("Company Post")
        verbose_name_plural = _("Company Posts")

    url_name = "company_post_detail"

    representative = models.ForeignKey(
        to="Representative",
        related_name="posts",
        verbose_name=_("Representative"),
        help_text=_("The owner of the share"),
        on_delete=models.CASCADE,
    )

    company_name = models.CharField(
        max_length=256,
        help_text=_("The company name"),
    )

    post_description = models.CharField(
        max_length=256,
        help_text=_("The description of the post"),
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return (
            f"{self.representative} is "
            f"{self.post_description} at {self.company_name} (PK: {self.pk})"
        )


class Issue(models.Model):
    """An issue is a theme of discussion between lobbysts and politician.

    The set of issues forms a classification through wich the navigation can be simplified.
    """

    class Meta:
        verbose_name = _("Issue")
        verbose_name_plural = _("Issues")

    url_name = "issue_detail"

    name = models.CharField(
        max_length=128,
        blank=True,
        null=True,
        help_text=_("The issue denomination"),
        unique=True,
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f"Issue {self.name} (PK: {self.pk})"


class Meeting(models.Model):
    """The meeting instance,
    one or more persons doing lobbying activity for organizations or on their own
    meet one or more politicians on a given date, to discuss a specific subject,
    that concerns one or more issues.
    """

    class Meta:
        verbose_name = _("Meeting")
        verbose_name_plural = _("Meetings")

    url_name = "meeting_detail"

    date = models.DateField(
        _("Meeting date"), help_text=_("The meeting date, with no time")
    )

    subject = models.CharField(
        verbose_name=_("Subject"),
        max_length=512,
        help_text=_("The subject being discussed in the meeting"),
    )

    representatives = models.ManyToManyField(
        verbose_name=_("Representatives"),
        to="Representative",
        related_name="meetings",
        help_text=_("Representatives present at the meeting"),
    )

    lobbyists = models.ManyToManyField(
        verbose_name=_("Lobbyists"),
        to="Lobbying",
        related_name="meetings",
        help_text=_("Lobbyists present at the meeting"),
    )

    issues = models.ManyToManyField(
        verbose_name=_("Issues"),
        to="Issue",
        related_name="meetings",
        help_text=_("Issues discussed in this meeting"),
    )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f"Meeting on {self.date} about {self.subject} (PK: {self.pk})"
