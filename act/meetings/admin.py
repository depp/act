from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from modeltranslation.admin import TabbedTranslationAdmin

from act.meetings.filters import MyDateTimeFilter
from act.meetings.models import (
    CompanyPost,
    CompanyShare,
    Issue,
    Lobbying,
    Meeting,
    Organization,
    Person,
    Representative,
)


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    model = Person
    list_display = ("given_name", "family_name", "gender")
    search_fields = ("family_name",)


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    model = Organization
    list_display = (
        "name",
        "org_type",
    )
    search_fields = ("name",)
    list_filter = ("org_type",)


@admin.register(Lobbying)
class LobbyingAdmin(admin.ModelAdmin):
    model = Lobbying
    list_display = ("person", "organization")
    search_fields = ("person__family_name", "organization__name")


class SharesInline(admin.TabularInline):
    extra = 0
    fields = ("company_name", "share_percentage")
    model = CompanyShare
    show_change_link = True


class PostsInline(admin.TabularInline):
    extra = 0
    fields = ("company_name", "post_description")
    model = CompanyPost
    show_change_link = True


@admin.register(Representative)
class RepresentativeAdmin(admin.ModelAdmin):
    model = Representative
    list_display = ("name", "party", "post_type")
    search_fields = ("family_name",)
    list_filter = ("party", "age_class", "post_type__descr")
    inlines = [SharesInline, PostsInline]

    def name(self, obj):
        return f"{obj.given_name} {obj.family_name}"

    name.short_description = _("Full name")
    name.admin_order_field = "family_name"


@admin.register(Issue)
class IssueAdmin(TabbedTranslationAdmin):
    model = Issue


@admin.register(Meeting)
class MeetingAdmin(admin.ModelAdmin):
    model = Meeting
    list_display = ("date", "subject", "participants")
    search_fields = (
        "representative__family_name",
        "lobbying__person__family_name",
        "lobbying__organization__name",
    )
    list_filter = (("date", MyDateTimeFilter), "issues")
    filter_horizontal = ("representatives", "lobbyists", "issues")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("issues", "representatives", "lobbyists")

    def participants(self, obj):

        i18n_for = _("for")
        i18n_and = _("and")
        i18n_alone = _("alone")
        i18n_metby = _("met by")

        representatives_list = [
            f"{r.given_name} {r.family_name}" for r in obj.representatives.all()
        ]
        n_representatives = obj.representatives.count()
        representatives = ", ".join(representatives_list[:-1])
        if n_representatives > 1:
            representatives += f" {_('and')} "
        representatives += f"{representatives_list[-1]}"

        lobbyists_list = []
        n_lobbyists = obj.lobbyists.count()
        for lobbyist in obj.lobbyists.all():
            if lobbyist.organization:
                lobbyists_list.append(
                    f"{lobbyist.person.family_name} ({i18n_for} {lobbyist.organization.name})"
                )
            else:
                lobbyists_list.append(f"{lobbyist.person.family_name} ({i18n_alone})")
        lobbyists = ", ".join(lobbyists_list[:-1])
        if n_lobbyists > 1:
            lobbyists += f" {i18n_and} "
        lobbyists += f"{lobbyists_list[-1]}"

        return f"{representatives} {i18n_metby} {lobbyists}"

    participants.short_description = _("Participants")
