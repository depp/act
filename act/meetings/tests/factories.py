import random

import factory
from factory import Faker
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice

from act.meetings.models import (
    CompanyPost,
    CompanyShare,
    Issue,
    Meeting,
    Organization,
    Person,
    PostType,
    Representative,
)


class PersonFactory(DjangoModelFactory):

    gender = "M"  # or 'F' or 'nonbinary'
    family_name = Faker("last_name", locale="it")

    @factory.lazy_attribute
    def given_name(self):
        if self.gender[0].lower() == "m":
            faker_provider = "first_name_male"
        elif self.gender[0].lower() == "f":
            faker_provider = "first_name_female"
        else:
            faker_provider = "first_name_nonbinary"

        return Faker(faker_provider).generate(params={"locale": "it"})

    class Meta:
        model = Person
        django_get_or_create = ["given_name", "family_name"]


class OrganizationFactory(DjangoModelFactory):

    name = Faker("company", locale="it")
    org_type = factory.LazyAttribute(lambda a: random.choice(["PRI", "PUB", "LOB"]))

    class Meta:
        model = Organization
        django_get_or_create = ["name"]


class CompanyShareFactory(DjangoModelFactory):
    class Meta:
        model = CompanyShare
        django_get_or_create = ["representative", "company_name"]

    representative = None
    company_name = Faker("company", locale="it")
    share_percentage = Faker("pyfloat", min_value=0.0, max_value=100.0, right_digits=2)


class CompanyPostFactory(DjangoModelFactory):
    class Meta:
        model = CompanyPost
        django_get_or_create = ["representative", "company_name"]

    representative = None
    company_name = Faker("company", locale="it")
    post_description = Faker("sentence", locale="it")


class PostTypeFactory(DjangoModelFactory):
    class Meta:
        model = PostType
        django_get_or_create = ["descr"]

    descr = Faker("word", locale="it")


class RepresentativeFactory(DjangoModelFactory):
    class Meta:
        model = Representative
        django_get_or_create = ["given_name", "family_name"]

    gender = "M"  # or 'F' or 'nonbinary'
    family_name = Faker("last_name", locale="it")

    @factory.lazy_attribute
    def given_name(self):
        if self.gender[0].lower() == "m":
            faker_provider = "first_name_male"
        elif self.gender[0].lower() == "f":
            faker_provider = "first_name_female"
        else:
            faker_provider = "first_name_nonbinary"

        return Faker(faker_provider).generate(params={"locale": "it"})

    age_class = FuzzyChoice(Representative.AgeClasses.choices, getter=lambda c: c[0])
    party = FuzzyChoice(
        ["Compagni", "Centrosinistra", "Centrodestra", "Fasci", "Imbecilli"]
    )
    post_type = FuzzyChoice(PostType.objects.all())

    @factory.lazy_attribute
    def post_start_date(self):
        return (
            Faker("date_between")
            .generate(params={"start_date": "-3y", "end_date": "-6m", "locale": "it"})
            .strftime("%Y-%m-%d")
        )

    @factory.lazy_attribute
    def post_end_date(self):
        if random.choice([0, 0, 1]):
            return (
                Faker("date_between")
                .generate(params={"start_date": "-6m", "locale": "it"})
                .strftime("%Y-%m-%d")
            )
        else:
            return None

    @factory.lazy_attribute
    def last_income(self):
        return (
            random.choice([0, 8, 15, 25, 35, 40, 45, 50, 60, 65, 70, 75, 80, 100])
            * 1000
        )

    shares = factory.RelatedFactoryList(
        CompanyShareFactory,
        factory_related_name="representative",
        size=lambda: random.choice([0, 0, 0, 0, 1, 1, 2]),
    )

    posts = factory.RelatedFactoryList(
        CompanyPostFactory,
        factory_related_name="representative",
        size=lambda: random.choice([0, 0, 0, 1, 1, 2]),
    )


class IssueFactory(DjangoModelFactory):
    name = Faker("word", locale="it")

    class Meta:
        model = Issue
        django_get_or_create = ["name"]


class MeetingFactory(DjangoModelFactory):
    class Meta:
        model = Meeting

    date = Faker("date_between", start_date="-3y", end_date="-6m")
    subject = Faker("sentence", locale="it")

    @factory.post_generation
    def issues(self, create, extracted):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for issue in extracted:
                self.issues.add(issue)

    @factory.post_generation
    def representatives(self, create, extracted):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for representative in extracted:
                self.representatives.add(representative)

    @factory.post_generation
    def lobbyists(self, create, extracted):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for lobbyist in extracted:
                self.lobbyists.add(lobbyist)
