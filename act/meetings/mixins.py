from django.conf import settings
from django.db import models
from django.db.models.aggregates import Count
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

import act.meetings.models


class MeetingsDetailsMixin:
    """
    An abstract base class model that provides method to extract details about meetings of an instance
    """

    meetings = None

    class Meta:
        abstract = True

    @property
    def n_future_meetings(self):
        return self.meetings.filter(date__gte=now()).count()

    @property
    def meetings_timeline_monthly(self):
        return (
            self.meetings.values("date__year", "date__month")
            .annotate(n=Count(("date__year", "date__month")))
            .values("date__year", "date__month", "n")
            .order_by("date__year", "date__month")
        )

    @property
    def meetings_pks(self):
        return self.meetings.values_list("pk", flat=True)

    @property
    def issues_cloud(self):
        return (
            act.meetings.models.Issue.objects.filter(meetings__in=self.meetings_pks)
            .annotate(n=Count("meetings"))
            .values("name", "n")
        )

    @property
    def persons_organizations_met(self):
        return act.meetings.models.Lobbying.objects.filter(
            meetings__date__lt=now(), meetings__in=self.meetings_pks
        )

    @property
    def organizations_met(self):
        return act.meetings.models.Organization.objects.filter(
            id__in=act.meetings.models.Lobbying.objects.filter(
                meetings__date__lt=now(), meetings__in=self.meetings_pks
            )
            .values("organization_id")
            .distinct()
        )

    @property
    def last_n_meetings(self):
        return self.meetings.filter(date__lt=now()).order_by("-date")[: settings.N_TOP]

    @property
    def next_n_meetings(self):
        return self.meetings.filter(date__gte=now()).order_by("date")[: settings.N_TOP]


class PrioritizedModel(models.Model):
    """
    An abstract base class that provides an optional priority field,
    to impose a custom sorting order.
    """

    priority = models.IntegerField(
        _("Priority"),
        null=True,
        blank=True,
        default=0,
        help_text=_("Sort order in case ambiguities arise"),
    )

    class Meta:
        abstract = True
