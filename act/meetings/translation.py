from modeltranslation.translator import TranslationOptions, register

from .models import Issue


@register(Issue)
class IssueTranslationOptions(TranslationOptions):
    fields = ("name",)
