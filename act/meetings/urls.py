from django.urls import path

from act.meetings.views import (
    meeting_list_view,
    organization_detail_view,
    organization_list_view,
    representative_detail_view,
    representative_list_view,
)

app_name = "meetings"
urlpatterns = [
    path("representatives/", view=representative_list_view, name="representative_list"),
    path(
        "representatives/<int:pk>/",
        view=representative_detail_view,
        name="representative_detail",
    ),
    path("organizations/", view=organization_list_view, name="organization_list"),
    path(
        "organizations/<int:pk>/",
        view=organization_detail_view,
        name="organization_detail",
    ),
    path("meetings/", view=meeting_list_view, name="meeting_list"),
]
