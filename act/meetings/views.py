from datetime import date, datetime
from typing import Any, Dict

from django.conf import settings
from django.db.models.aggregates import Avg, Count
from django.db.models.expressions import F
from django.urls import reverse
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from act.meetings.models import Issue, Meeting, Organization, Representative
from act.presentations.models import Presentations


class HomeView(TemplateView):

    slug_field = "home"
    slug_url_kwarg = "home"
    template_name = "pages/home.html"

    @staticmethod
    def annotate_top_representatives_by_interests(n_top=10):
        """It's a complex extraction that cannot be performed by the ORM

        :param n_top:
        :return: a list of Representative objects, annotated with 'n' field
        """
        tuples = sorted(
            [
                (
                    r,
                    len(
                        set(r.shares.values_list("company_name", flat=True))
                        | set(r.posts.values_list("company_name", flat=True))
                    ),
                )
                for r in Representative.objects.all()
            ],
            key=lambda x: x[1],
            reverse=True,
        )[:n_top]
        objects = []
        for t in tuples:
            o = t[0]
            o.n = t[1]
            objects.append(o)
        return objects

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        n_top = settings.N_TOP
        context.update(
            {
                "presentation_texts": Presentations.objects.first(),
                "meetings": {
                    "first_date": Meeting.objects.order_by("date").first().date,
                    "n_up_to_now": Meeting.objects.filter(
                        date__lt=datetime.now()
                    ).count(),
                    "n_from_now_on": Meeting.objects.filter(
                        date__gte=datetime.now()
                    ).count(),
                    "top_reprs": Representative.objects.annotate(
                        n=Count("meetings")
                    ).order_by("-n")[:n_top],
                    "top_reprs_all_url": reverse("meetings:representative_list") + "",
                    "top_orgs": Organization.objects.annotate(
                        n=Count("lobbyings__meetings")
                    ).order_by("-n")[:n_top],
                    "top_orgs_all_url": reverse("meetings:organization_list") + "",
                    "timeline_monthly": Meeting.objects.values(
                        "date__year", "date__month"
                    )
                    .annotate(n=Count(("date__year", "date__month")))
                    .values("date__year", "date__month", "n")
                    .order_by("date__year", "date__month"),
                    "timeline_weekly": Meeting.objects.values(
                        "date__year", "date__month", "date__week"
                    )
                    .annotate(n=Count(("date__year", "date__month", "date__week")))
                    .values("date__year", "date__month", "date__week", "n")
                    .order_by("date__year", "date__month", "date__week"),
                    "issues_cloud": Issue.objects.annotate(n=Count("meetings")).values(
                        "name", "n"
                    ),
                    "last_n": Meeting.objects.filter(date__lt=date.today()).order_by(
                        "-date"
                    )[:n_top],
                    "next_n": Meeting.objects.filter(date__gte=date.today()).order_by(
                        "date"
                    )[:n_top],
                    "last_n_all_url": reverse("meetings:meeting_list")
                    + "#"
                    + '{"col_1"%3A{"flt"%3A"<%3D'
                    + datetime.now().strftime("%Y-%m-%d")
                    + '"%2C"sort"%3A{"descending"%3Atrue}}}',
                    "next_n_all_url": reverse("meetings:meeting_list")
                    + "#"
                    + '{"col_1"%3A{"flt"%3A">'
                    + datetime.now().strftime("%Y-%m-%d")
                    + '"%2C"sort"%3A{"descending"%3Afalse}}}',
                },
                "interests": {
                    "n_representatives": Representative.objects.count(),
                    "avg_income": Representative.objects.aggregate(
                        av=Avg("last_income")
                    )["av"],
                    "n_organizations_of_interest": len(
                        set(
                            Representative.objects.filter(
                                shares__isnull=False
                            ).values_list("shares__company_name", flat=True)
                        )
                        | set(
                            Representative.objects.filter(
                                posts__isnull=False
                            ).values_list("posts__company_name", flat=True)
                        )
                    ),
                    "top_reprs_by_income": Representative.objects.annotate(
                        n=F("last_income")
                    ).order_by("-last_income")[:n_top],
                    "top_reprs_by_income_all_url": reverse(
                        "meetings:representative_list"
                    )
                    + "",
                    "top_reprs_by_interests": self.annotate_top_representatives_by_interests(
                        n_top
                    ),
                    "top_reprs_by_interests_all_url": reverse(
                        "meetings:representative_list"
                    )
                    + "",
                },
            }
        )
        return context


class RepresentativeListView(ListView):
    model = Representative
    paginate_by = 100

    def get_queryset(self):
        """Annotate all representatives with the n. of meetings and interests"""

        qs = super().get_queryset()
        qs = qs.annotate(n_meetings=Count("meetings"))
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "presentation_text": Presentations.objects.first(),
            }
        )
        return context


class RepresentativeDetailView(DetailView):
    model = Representative

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        obj = self.get_object()
        context.update(
            {
                "timeline_monthly": obj.meetings.values("date__year", "date__month")
                .annotate(n=Count(("date__year", "date__month")))
                .values("date__year", "date__month", "n")
                .order_by("date__year", "date__month"),
                "issues_cloud": obj.meetings.filter(issues__isnull=False)
                .values("issues__name")
                .annotate(n=Count("issues__name"))
                .annotate(name=F("issues__name"))
                .values("name", "n"),
            }
        )
        return context


class OrganizationListView(ListView):
    model = Organization
    paginate_by = 100

    def get_queryset(self):
        """Annotate all representatives with the n. of meetings and interests"""

        qs = super().get_queryset()
        qs = qs.annotate(n_meetings=Count("lobbyings__meetings"))
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "presentation_text": Presentations.objects.first(),
                "org_types_data": [
                    {
                        "org_type": getattr(
                            Organization.OrgTypes, f"ORG_{ot[0]}"
                        ).label,
                        "n_organizations": ot[1],
                    }
                    for ot in Organization.objects.values_list("org_type").annotate(
                        n=Count("org_type")
                    )
                ],
            }
        )
        return context


class OrganizationDetailView(DetailView):
    model = Organization

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        obj = self.get_object()
        context.update(
            {
                "timeline_monthly": obj.meetings.values("date__year", "date__month")
                .annotate(n=Count(("date__year", "date__month")))
                .values("date__year", "date__month", "n")
                .order_by("date__year", "date__month"),
                "issues_cloud": obj.meetings.filter(issues__isnull=False)
                .values("issues__name")
                .annotate(n=Count("issues__name"))
                .annotate(name=F("issues__name"))
                .values("name", "n"),
            }
        )
        return context


class MeetingListView(ListView):
    model = Meeting
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "presentation_text": Presentations.objects.first(),
            }
        )

        return context


home_view = HomeView.as_view()
representative_list_view = RepresentativeListView.as_view()
representative_detail_view = RepresentativeDetailView.as_view()
organization_list_view = OrganizationListView.as_view()
organization_detail_view = OrganizationDetailView.as_view()
meeting_list_view = MeetingListView.as_view()
