from modeltranslation.translator import TranslationOptions, register

from .models import Presentations


@register(Presentations)
class PresentationsTranslationOptions(TranslationOptions):
    fields = (
        "home_title",
        "home_text",
        "representatives_text",
        "meetings_text",
        "organizations_text",
    )
