# Generated by Django 3.0.10 on 2021-02-09 11:42

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0007_auto_20210209_1236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentations',
            name='home_text',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Text in Home page'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='presentations',
            name='home_title',
            field=models.CharField(blank=True, default='Anti Corruption Toolkit', max_length=255, verbose_name='Title in Home page'),
        ),
        migrations.AlterField(
            model_name='presentations',
            name='meetings_text',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Text in Meetings page'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='presentations',
            name='organizations_text',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Text in Organizations page'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='presentations',
            name='representatives_text',
            field=ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Text in Representatives page'),
            preserve_default=False,
        ),
    ]
