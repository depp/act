from django.contrib import admin
from modeltranslation.admin import TabbedTranslationAdmin

from act.presentations.models import Presentations


@admin.register(Presentations)
class PresentationsAdmin(TabbedTranslationAdmin):
    model = Presentations
