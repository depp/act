from ckeditor.fields import RichTextField
from django.db.models.fields import CharField
from django.utils.translation import ugettext_lazy as _

from act.utils.models import SingletonModel


class Presentations(SingletonModel):
    """Specfic presentation texts for the home page, the representatives, organizations and meetings pages"""

    class Meta:
        verbose_name = _("Presentation texts")
        verbose_name_plural = _("Presentation texts")

    home_title = CharField(
        _("Title in Home page"),
        blank=True,
        max_length=255,
        default=_("Anti Corruption Toolkit"),
    )
    home_text = RichTextField(
        _("Text in Home page"),
        blank=True,
    )
    representatives_text = RichTextField(
        _("Text in Representatives page"),
        blank=True,
    )
    meetings_text = RichTextField(
        _("Text in Meetings page"),
        blank=True,
    )
    organizations_text = RichTextField(
        _("Text in Organizations page"),
        blank=True,
    )

    def __str__(self):
        return "{}".format(_("Presentations texts"))
