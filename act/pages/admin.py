from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site
from django.forms.widgets import MultipleHiddenInput
from modeltranslation.admin import TabbedTranslationAdmin


class CustomFlatpageAdminForm(FlatpageForm):
    class Meta:
        widgets = {
            "content_it": CKEditorWidget(),
            "content_en": CKEditorWidget(),
            "sites": MultipleHiddenInput(),
        }


admin.site.unregister(FlatPage)


@admin.register(FlatPage)
class CustomFlatPageAdmin(FlatPageAdmin, TabbedTranslationAdmin):
    form = CustomFlatpageAdminForm
    list_filter = []

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        if request.user.is_superuser:
            list_display += ("template_name",)
        return list_display

    def get_fieldsets(self, request, obj=None):
        fields = ("url", "title", "content", "meta_description", "sites")
        if request.user.is_superuser:
            fields += ("template_name",)
        self.fieldsets = ((None, {"fields": fields}),)
        return super().get_fieldsets(request, obj)

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser and obj:
            return tuple(self.readonly_fields) + ("url",)
        return super().get_readonly_fields(request, obj)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "sites":
            kwargs["initial"] = [Site.objects.get_current()]
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
