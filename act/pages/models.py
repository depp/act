from django.contrib.flatpages.models import FlatPage
from django.db import models
from django.utils.translation import ugettext_lazy as _

FlatPage.add_to_class(
    "meta_description", models.TextField(_("Metadata description for SEO"), blank=True)
)
