from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = "act.pages"
