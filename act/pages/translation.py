from django.contrib.flatpages.models import FlatPage
from modeltranslation.translator import TranslationOptions, register


@register(FlatPage)
class FlatPageTranslationOptions(TranslationOptions):
    fields = (
        "title",
        "meta_description",
        "content",
    )
