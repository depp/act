from datetime import datetime

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

partial_date_validator = RegexValidator(
    regex="^[0-9]{4}(-[0-9]{2}){0,2}$", message=_("Date has wrong format")
)


def validate_percentage(value):
    if value < 0.0 or value > 100.0:
        raise ValidationError(_(f"{value} is not a percentage"))


def validate_income(value):
    if value < 0.0:
        raise ValidationError(_("Income must be positive or zero"))

    if value > 10e9:
        raise ValidationError(_("Jeff Bezos is not a politician"))


def validate_partial_date(value):
    """
    Validate a partial date, it can be partial, but it must yet be a valid date.
    Accepted formats are: YYYY-MM-DD, YYYY-MM, YYYY.
    2013-22 must rais a ValidationError, as 2013-13-12, or 2013-11-55.
    """
    try:
        datetime.strptime(value, "%Y-%m-%d")
    except ValueError:
        try:
            datetime.strptime(value, "%Y-%m")
        except ValueError:
            try:
                datetime.strptime(value, "%Y")
            except ValueError:
                raise ValidationError(_(f"date seems not to be correct: {value}"))
