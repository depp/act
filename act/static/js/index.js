/*Grafico linea*/
document.addEventListener('DOMContentLoaded', () =>  {
    const options = {
        xAxis: {
           type: 'category',
                lineColor: '#d6d6d6',
                tickColor: '#d6d6d6',
                labels: {
                rotation: 0,
                    style: {
                    fontSize: '12px'
                }
            }
        },
        yAxis: {
            title: {
                enabled: false,
              },
            gridLineWidth: 1,
          },
        plotOptions: {
            series: {
                allowPointSelect: true,
                marker: {
                  enabled: false,
                },
              },
            line: {
                dataLabels: {
                    verticalAlign: 'bottom',
                        enabled: false,
                        y: -5,
                        style: {
                            fontSize: '14px',
                            color:  '#27A6B5'
                        }
                },
                marker: {
                  states: {
                    select: {
                      fillColor: '#F8C630',
                      lineWidth: 0
                    }
                  },
                    fillColor: '#F8C630',
                }
            }
        },
        tooltip: {
            pointFormat: 'Incontri: <span style=\'font-weight: bold; color: #27A6B5\'>{point.y}</span>',
            backgroundColor: '#ffffff',
            borderColor: '#27A6B5',
            borderRadius: 7,
            borderWidth: 2,
            distance: 20,
            outside: true,
            shadow: false,
            style: {
            color: '#333333',
            fontSize: '14px',
            },
        },
        colors: [
            '#27A6B5'
        ],
        title: {
            text: '',
            align: 'left',
            x: 10
        },
        subtitle: {
            text: '',
            align: 'left',
            x: 10
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false,
        },
        credits: {
            enabled: false
        },
        data: {
            table: 'andamento-datatable'
        },
        chart: {
           type: 'line',
           renderTo: 'container',
           spacingTop: 1,
           style: {
               fontFamily: '',
           },
           events: {
               load: function () {
                   var M = {
                       '1': 'g',
                       '2': 'f',
                       '3': 'm',
                       '4': 'a',
                       '5': 'm',
                       '6': 'g',
                       '7': 'l',
                       '8': 'a',
                       '9': 's',
                       '10': 'o',
                       '11': 'n',
                       '12': 'd'
                   };
                   var xAxis = this.xAxis['0'];
                   var groupedCategories = [];
                   var month;
                   var year;
                   var yearGroup;
                   var currentYear;
                   var itemArr;
                   for (var idx = 0; idx < xAxis.names.length; idx++) {
                       itemArr = xAxis.names[idx].split('/');
                       month = itemArr[0];
                       year = itemArr[1];
                       if (yearGroup != year) {
                           yearGroup = year;
                           currentYear = {name: year, categories: []};
                           groupedCategories.push(currentYear);
                           xAxis.addPlotLine({color: '#d6d6d6', value: idx - 0.5, width: 0});
                       }
                       currentYear.categories.push(M[month]);
                   }
                   xAxis.update({categories: groupedCategories});
               }
           }
        },
    }
    Highcharts.chart('andamento-incontri', options);
  });

/*Grafico tagcloud*/
document.addEventListener('DOMContentLoaded', () =>  {
    const options = {
        title: {
            text: '',
            align: 'left',
            x: 10
        },
        subtitle: {
            text: '',
            align: 'left',
            x: 10
        },
        data: {
            table: 'keywords-datatable'
        },
        exporting: {
            enabled: false,
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                allowPointSelect: true,
                states: {
                    select: {
                        color: '#F8C630',
                    },
                    hover: {
                        color: '#F8C630',
                    }
                },
                marker: {
                    enabled: false
                },
                lineWidth: 3
            }
        },
        colors: ['#27A6B5'],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            itemMarginBottom: 10,
            x: 0,
            symbolRadius: 2,
            enabled: false
        },
        chart: {
            type: 'wordcloud',
            style: {
                fontFamily: '',
            },
        },
        series: [
            {
                turboThreshold: 0,
                _colorIndex: 0,
                _symbolIndex: 0,
                type: 'wordcloud'
            }
        ],
        tooltip: {
            enabled: false,
            shared: true,
        },
        navigation: {
            events: {
                'showPopup': 'function(e){this.chart.indicatorsPopupContainer||(this.chart.indicatorsPopupContainer=document.getElementsByClassName(\'highcharts-popup-indicators\')[0]),this.chart.annotationsPopupContainer||(this.chart.annotationsPopupContainer=document.getElementsByClassName(\'highcharts-popup-annotations\')[0]),\'indicators\'===e.formType?this.chart.indicatorsPopupContainer.style.display=\'block\':\'annotation-toolbar\'===e.formType&&(this.chart.activeButton||(this.chart.currentAnnotation=e.annotation,this.chart.annotationsPopupContainer.style.display=\'block\')),this.popup&&(t=this.popup)}',
                'closePopup': 'function(){this.chart.annotationsPopupContainer.style.display=\'none\',this.chart.currentAnnotation=null}',
                'selectButton': 'function(e){var t=e.button.className+\' highcharts-active\';e.button.classList.contains(\'highcharts-active\')||(e.button.className=t,this.chart.activeButton=e.button)}',
                'deselectButton': 'function(e){e.button.classList.remove(\'highcharts-active\'),this.chart.activeButton=null}'
            },
            bindingsClassName: 'tools-container'
        }
    }

    Highcharts.chart('keywords', options);
});

/*Animazione del bottone del menu*/
$(document).ready(function () {

  $('.second-button').on('click', function () {

    $('.animated-icon2').toggleClass('open');
  });

});
