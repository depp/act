Setup language e weblate
========================

Ho assegnato alla variabile ``settings.LANGUAGES`` i valori italiano e inglese,
e ho aggiunto uno snippet HTML al template ``base.html``, che permette di selezionare la lingua.

Queste impostazioni vanno configurate esclusivamente nei settings,
per permettere alle tre istanze di partire con coppie di lingue distinte (it, en), (es, en),  (el, en).

Le traduzioni sono centralizzate su https://staging.weblate.openpolis.it (https://weblate.openpolis.it/)
(passeranno su https://weblate.openpolis.it (https://weblate.openpolis.it/)).

Sono state create agganciando weblate al repository gitlab del progetto ACT, su un branch distinto (translations).
Le modifiche ai files .po introdotte con makemessages, vengono prese dal repository (sezione gestione),
mentre le nuove traduzioni possono essere inviate e pushate automaticamente. Tutto avviene manualmente,
tramite l’interfaccia di gestione del componente ACT-webapp, del progetto ACT.

In questo modo è possibile dare un account ai traduttori, che possono inserire le traduzioni in un sistema unico,
senza che vengano passati files .po e si incasini la situazione.


Procedura per quando i traduttori hanno aggiunto messaggi
---------------------------------------------------------

L'invio dei messaggi tradotti avviene quando io, in Weblate attivo il **Commit**. I files `.po` finiscono nelk branch
`translations`.

A questo punto ci sono due scenari, a seconda se, nel frattempo, i messaggi *source*,
ovvero le stringe chiave dei files `.po`, in inglese, sono cambiati o no.

I messaggi source non sono cambiati
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- andare sul branch translations in gitlab (repository > commits [branch translations])
- creare una merge request (**senza il delete del source branch**)
- approve + merge della MR
- in 40 secondi circa, le traduzioni sono online
- pull del merge in sviluppo


I messaggi source sono cambiati
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- entrare nel virtualenv di sviluppo
- rigenerare i messaggi nei files .po

  .. code-block:: bash

    python manage.py makemessages --ignore venv --ignore act/templates/account --ignore act/users

- git pull
- git checkout translations
- git pull origin translations
- git merge master
- risoluzione dei conflitti eventuali
- git commit -m "messaggio originale di weblate"
- git push origin translations
- git checkout master
- git merge translations
- git push
- in 40 secondi circa, le traduzioni sono online


