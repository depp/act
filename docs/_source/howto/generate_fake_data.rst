Generazione dati di prova
=========================

È possibile generare dei dati di prova per effettuare il test di viste e template, prima
di avere a disposizione i dati reali.

Basta lanciare il task ``generate_fake_data``, passando il parametro ``--db-reset``.

.. code-block:: bash

    generate_fake_data --db-reset --fixtures-file=./fixtures.json


Il parametro ``--fixtures`` non è obbligatorio e serve a produrre dei dati di fixture, che possono essere
caricati in un nuovo sistema, senza dover rilanciare la procedura di generazione.