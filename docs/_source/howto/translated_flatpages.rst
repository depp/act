Flatpages e traduzioni
======================

L'applicazione `flatpage` non viene intercettata da `django-modeltranslation`.

Questo documento descrive un trucco per fare in modo che il modello e il sito `admin` di django
possano lavorare con le flatpages come con un qualsiasi modello, usando le traduzioni.

Dopo aver installato l'applicazione `flatpage`, occorre creare una applicazione interna (ad es: `pages`),
e:
- modificare il `models.py`, aggiungendo eventuali campi;
- modificare *solamente* il file admin.py, facendo in modo che erediti da `TabbedTranslationAdmin`;
- aggiungere un file `translation.py`, per registrare il modello con `django-modeltranslation` e creare le tabelle.

Le migrazioni sono state spostate (nei settings), nel modulo `act.pages`. Occorre eseguire le migrazioni standard
di flatpages usando l'opzione `--fake`, e poi eseguire le migrazioni del modulo page, altrimenti va in errore.

settings.py:

.. code-block:: python

    MIGRATION_MODULES = {
        "sites": "act.contrib.sites.migrations",
        "flatpages": "act.pages.migrations",
    }


models.py:

.. code-block:: python

    FlatPage.add_to_class('meta_description', models.TextField(_('Metadata description for SEO'), blank=True))


admin.py:

.. code-block:: python

    class CustomFlatpageAdminForm(FlatpageForm):
        class Meta:
            widgets = {
                'content_it': CKEditorWidget(),
                'content_en': CKEditorWidget(),
                'sites': MultipleHiddenInput()
            }


    admin.site.unregister(FlatPage)


    @admin.register(FlatPage)
    class CustomFlatPageAdmin(FlatPageAdmin, TabbedTranslationAdmin):
        form = CustomFlatpageAdminForm
        list_filter = []

        def get_list_display(self, request):
            list_display = super().get_list_display(request)
            if request.user.is_superuser:
                list_display += ('template_name',)
            return list_display

        def get_fieldsets(self, request, obj=None):
            fields = ('url', 'title', 'content', 'meta_description', 'sites')
            if request.user.is_superuser:
                fields += ('template_name',)
            self.fieldsets = ((None, {'fields': fields}),)
            return super().get_fieldsets(request, obj)

        def get_readonly_fields(self, request, obj=None):
            if not request.user.is_superuser and obj:
                return tuple(self.readonly_fields) + ('url',)
            return super().get_readonly_fields(request, obj)

        def formfield_for_manytomany(self, db_field, request, **kwargs):
            if db_field.name == 'sites':
                kwargs['initial'] = [Site.objects.get_current()]
            return super().formfield_for_foreignkey(db_field, request, **kwargs)



translation.py:

.. code-block:: python

    @register(FlatPage)
    class FlatpageTranslationOptions(TranslationOptions):
        fields = ('title', 'content', )


In questo modo è possibile estendere la flatpage, rendere i suoi campi traducibili, e avere il widget cool
con il tab multi-lingua nell'admin.