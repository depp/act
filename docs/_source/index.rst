.. ACT documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentazione di progetto
==========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   howto/generate_fake_data
   howto/translations_weblate
   howto/translated_flatpages
